-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 04 mai 2018 à 09:50
-- Version du serveur :  5.7.21
-- Version de PHP :  5.6.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projetpiscine`
--

-- --------------------------------------------------------

--
-- Structure de la table `amis`
--

DROP TABLE IF EXISTS `amis`;
CREATE TABLE IF NOT EXISTS `amis` (
  `id_amis` int(10) NOT NULL AUTO_INCREMENT,
  `nom_utilisateur` int(30) NOT NULL,
  `utilisateur_lie` varchar(30) NOT NULL,
  PRIMARY KEY (`id_amis`),
  KEY `Amities` (`utilisateur_lie`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `amis`
--

INSERT INTO `amis` (`id_amis`, `nom_utilisateur`, `utilisateur_lie`) VALUES
(3, 2, 'Nico91'),
(4, 3, 'Nico91');

-- --------------------------------------------------------

--
-- Structure de la table `cv`
--

DROP TABLE IF EXISTS `cv`;
CREATE TABLE IF NOT EXISTS `cv` (
  `id_cv` int(11) NOT NULL AUTO_INCREMENT,
  `date_creation` date DEFAULT NULL,
  `mur_lie` int(10) NOT NULL,
  PRIMARY KEY (`id_cv`),
  KEY `Affichage3` (`mur_lie`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `cv`
--

INSERT INTO `cv` (`id_cv`, `date_creation`, `mur_lie`) VALUES
(1, '2018-05-01', 1),
(2, '2018-05-01', 2);

-- --------------------------------------------------------

--
-- Structure de la table `evenement`
--

DROP TABLE IF EXISTS `evenement`;
CREATE TABLE IF NOT EXISTS `evenement` (
  `id_evenement` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `lieu` varchar(30) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `heure` time(6) DEFAULT NULL,
  `visibilite` varchar(30) DEFAULT NULL,
  `aime` int(10) DEFAULT NULL,
  `partage` int(10) DEFAULT NULL,
  `mur_lie` int(10) NOT NULL,
  PRIMARY KEY (`id_evenement`),
  KEY `Affichage4` (`mur_lie`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `evenement`
--

INSERT INTO `evenement` (`id_evenement`, `titre`, `description`, `lieu`, `date`, `heure`, `visibilite`, `aime`, `partage`, `mur_lie`) VALUES
(1, 'Dejeuner', 'Déjeuner avec les collègues', 'Versailles', '2018-05-01', '12:00:00.000000', 'Privé', 0, 0, 1),
(2, 'Réunion', 'Réunion pour le projet Piscine', 'ECE Paris', '2018-05-01', '08:00:00.000000', 'Publique', 3, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `mur`
--

DROP TABLE IF EXISTS `mur`;
CREATE TABLE IF NOT EXISTS `mur` (
  `id_mur` int(10) NOT NULL AUTO_INCREMENT,
  `date_creation` date DEFAULT NULL,
  `utilisateur_lie` varchar(30) NOT NULL,
  PRIMARY KEY (`id_mur`),
  KEY `murage` (`utilisateur_lie`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `mur`
--

INSERT INTO `mur` (`id_mur`, `date_creation`, `utilisateur_lie`) VALUES
(1, '2018-05-01', 'Nico91'),
(2, '2018-05-01', 'Aug78');

-- --------------------------------------------------------

--
-- Structure de la table `photo`
--

DROP TABLE IF EXISTS `photo`;
CREATE TABLE IF NOT EXISTS `photo` (
  `id_photo` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `lieu` varchar(30) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `heure` time(6) DEFAULT NULL,
  `ressenti` varchar(30) DEFAULT NULL,
  `visibilite` varchar(30) DEFAULT NULL,
  `aime` int(10) DEFAULT NULL,
  `partage` int(10) DEFAULT NULL,
  `mur_lie` int(10) NOT NULL,
  PRIMARY KEY (`id_photo`),
  KEY `Affiche1` (`mur_lie`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `photo`
--

INSERT INTO `photo` (`id_photo`, `titre`, `description`, `lieu`, `date`, `heure`, `ressenti`, `visibilite`, `aime`, `partage`, `mur_lie`) VALUES
(1, 'Photo de profil', 'Photo de profil', 'Toulouse', '2018-05-01', '15:00:00.000000', 'Heureux', 'Publique', 36, 0, 1),
(2, 'Photo d\'évènement', 'Photo d\'évènement', 'France', '2018-05-01', '09:00:00.000000', 'Impressioné', 'Privé', 5, 2, 1);

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `pseudo` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `date_de_naissance` date NOT NULL,
  `e_mail` varchar(50) NOT NULL,
  `mot_de_passe` varchar(30) NOT NULL,
  `statut` varchar(30) NOT NULL,
  `image_fond` varchar(255) DEFAULT NULL,
  `activite` varchar(30) NOT NULL,
  `photo` varchar(255) NOT NULL,
  PRIMARY KEY (`pseudo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`pseudo`, `nom`, `prenom`, `date_de_naissance`, `e_mail`, `mot_de_passe`, `statut`, `image_fond`, `activite`, `photo`) VALUES
('Aug78', 'Guermond', 'Augustin', '2018-05-01', 'Aug78@gmail.com', 'Aug', '', '0', 'etudiant', ''),
('Nico91', 'Vallat', 'Nicolas', '2018-05-01', 'nico91@gmail.com', 'nico', 'admin', '0', 'etudiant', ''),
('Sab93', 'Laleg', 'Sabry', '2018-05-01', 'Sab93@gmail.com', 'sab', '', '0', 'etudiant', '');

-- --------------------------------------------------------

--
-- Structure de la table `video`
--

DROP TABLE IF EXISTS `video`;
CREATE TABLE IF NOT EXISTS `video` (
  `id_video` int(10) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `description` varchar(200) DEFAULT NULL,
  `lieu` varchar(30) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `heure` time(6) DEFAULT NULL,
  `ressenti` varchar(30) DEFAULT NULL,
  `visibilite` varchar(30) DEFAULT NULL,
  `aime` int(11) DEFAULT NULL,
  `partage` int(11) DEFAULT NULL,
  `mur_lie` int(10) NOT NULL,
  PRIMARY KEY (`id_video`),
  KEY `Affichage2` (`mur_lie`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `video`
--

INSERT INTO `video` (`id_video`, `titre`, `description`, `lieu`, `date`, `heure`, `ressenti`, `visibilite`, `aime`, `partage`, `mur_lie`) VALUES
(1, 'CV Vidéo', 'Vidéo de présentation', 'Paris', '2018-05-01', '07:00:00.000000', 'Heureux', 'Publique', 10, 0, 2);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `amis`
--
ALTER TABLE `amis`
  ADD CONSTRAINT `Amities` FOREIGN KEY (`utilisateur_lie`) REFERENCES `utilisateur` (`pseudo`);

--
-- Contraintes pour la table `cv`
--
ALTER TABLE `cv`
  ADD CONSTRAINT `Affichage3` FOREIGN KEY (`mur_lie`) REFERENCES `mur` (`id_mur`);

--
-- Contraintes pour la table `evenement`
--
ALTER TABLE `evenement`
  ADD CONSTRAINT `Affichage4` FOREIGN KEY (`mur_lie`) REFERENCES `mur` (`id_mur`);

--
-- Contraintes pour la table `mur`
--
ALTER TABLE `mur`
  ADD CONSTRAINT `murage` FOREIGN KEY (`utilisateur_lie`) REFERENCES `utilisateur` (`pseudo`);

--
-- Contraintes pour la table `photo`
--
ALTER TABLE `photo`
  ADD CONSTRAINT `Affiche1` FOREIGN KEY (`mur_lie`) REFERENCES `mur` (`id_mur`);

--
-- Contraintes pour la table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `Affichage2` FOREIGN KEY (`mur_lie`) REFERENCES `mur` (`id_mur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
