<!DOCTYPE html>
<html>
<head>
	 <title>Accueil</title>
	 <meta charset="utf-8">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
</head>
<body>
 <div class="ui bottom attached segment pushable">
  <div class="ui inverted labeled icon left inline vertical sidebar menu uncover" style="">
    <a class="item">
    <i class="home icon"></i>
    Accueil                      
  </a>
  <a class="item">
    <i class="hand point down icon" type="button" onclick = "window.location='profil1.php'"></i>
   Vous                              
  </a>
  <a class="item">
    <i class="block layout icon"></i>
    Notifications                       
  </a>
  <a class="item">
    <i class="address book icon"></i>
    Mes Amis                               
  </a>
  <a class="item">
    <i class="smile icon"></i>
    Emplois                            
  </a>
  </div>
  
<div class="ui pusher ">
    <div class ="ui inverted block header" >
      <button class="ui button menui">  <i class="sidebar icon"></i>
            Menu </button> </a>
       <h2 style ="text-align: center;"> <i class="home icon"></i>Accueil</h2>
    </div>
</br>
</br>

	<div class = "ui stackable two column divided grid container">
	<div class="six wide column">
		<div class="ui people shape">
		  <div class="sides">
		    <div class="side active">
		      <div class="ui card">
		        <div class="image">
		          <i class="smile icon"></i>
		        </div>
		        <div class="content">
		          <div class="header">Vous</div>
		          <div class="meta">
		            <a>Emplois</a>
		          </div>
		          <div class="description">
		           Bio
		          </div>
		        </div>
		        <div class="extra content">
		          <span>
		            <i class="user icon"></i>
		            151 Friends
		          </span>
		        </div>
		      </div>
		    </div>
		</div>
	</div>
</div>
 <div class ="nine wide column">
 	<div class="ui container">
 		<div class="ui relaxed divided items">
		 <div class="ui input segment">
		  <input type="text" class="publish" placeholder="Publiez un événement." style = "height : 100px; width: 600px" />
		</div>
		</span>
		<div class ="ui grid">
		<div class ="right floated four wide column">
			<a class="item"><i class="image icon"></i></a>
		  <input type="button" value="Publier" name="publish" class="ui primary button" style = "height : 30px; width: 80px"  />
		  
		</div>

</div>
 	</div>
</div>
</div>



<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
<script >
	$('.menui').click(function(){
    $('.ui.sidebar').sidebar('toggle');
});</script>
</body>
</html>