<!DOCTYPE html>
<html>
<head>
	 <title>Inscription</title>
	 <meta charset="utf-8">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
</head>
<body>
<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <div class="content">
        Espace statutistrateur
      </div>
	  </br></br></br>
    </h2>
    <form class="ui large form" action="admin2.php" method="post">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="e_mail" name="e_mail" placeholder="adresse mail" />
          </div>
        </div>
		
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="text" name="pseudo" placeholder="pseudo" />
          </div>
        </div>	

        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="text" name="Prenom" placeholder="Prenom" />
          </div>
        </div>
     
      <input class="ui fluid large teal submit button" type="submit" name="action1" value="Inscription" />
	  </br></br></br>
	  <input class="ui fluid large teal submit button" type="submit" name="action2" value="Suppression" />
	  </br></br></br>
	  <input class="ui fluid large teal submit button" type="button" value="Retour" onclick = "window.location='interface_connexion.php'" />
	  
      </div>
     </form>
  </div>
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
</body>
</html>