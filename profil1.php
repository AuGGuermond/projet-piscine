<?php 
session_start(); //permet d'acceder aux infos de l'utilisateur recuperee dans verif_mp_e_mail
?>

<!DOCTYPE html>
<html>
<head>
	 <title>Mon Profil</title>
	 <meta charset="utf-8">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
</head>
<body>

 <div class="ui bottom attached segment pushable">
  <div class="ui inverted labeled icon left inline vertical sidebar menu uncover" style="">
    <a class="item">
    <i class="home icon" type="button" onclick = "window.location='index.php'"></i>
    Accueil                      
  </a>
  <a class="item">
    <i class="hand point down icon"></i>
   Vous                              
  </a>
  <a class="item">
    <i class="block layout icon"></i>
    Notifications                       
  </a>
  <a class="item">
    <i class="address book icon"></i>
    Mes Amis                               
  </a>
  <a class="item">
    <i class="smile icon"></i>
    Emplois                            
  </a>
  </div>
  
<div class="ui pusher ">
    <div class ="ui inverted block header" >
      <button class="ui button">  <i class="sidebar icon"></i>
            Menu </button> </a>
       <h2 style ="text-align: center;"> Mon profil</h2>
    </div>

  </br>
  </br>
<div class="ui container">
<center class ="ui">
  <img class="ui raised" src="pp.jpg" style = "height : 150px; width: 300px"/>
  <p> <?php echo '<img src = "'.$_SESSION['photo'].'"/>'?> </p>
  
</center>
<div class = "ui raised  blue  secondary segment">
 <a class="ui blue ribbon label">Mes informations </a>

 <center>
    <?php  echo '<p style ="text-align: center;"> <i class="circle icon"></i> Prenom : ' .$_SESSION['prenom'].' Nom : '.$_SESSION['nom'].'</p>'?>   
  <?php  echo '<p style ="text-align: center;"> <i class="circle icon"></i> Date de naissance : ' .$_SESSION['date_de_naissance'].'</p>'?>
<?php  echo '<p style ="text-align: center;"> <i class="circle icon"></i> Profession : ' .$_SESSION['activite'].'</p>'?>  
</center>


  </div>
  <div class = "ui raised blue secondary segment ">
     <a class="ui red ribbon label">CV et Expériences</a>
  </div>
</div>
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
<script>
$('.ui.button').click(function(){
     $('.ui.sidebar').sidebar('toggle');
});
  </script>

</body>
</html>