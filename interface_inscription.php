<!DOCTYPE html>
<html>
<head>
	 <title>Inscription</title>
	 <meta charset="utf-8">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
</head>
<body>
<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <div class="content">
        Inscrivez-vous pour accéder au site Linkdin ECE
      </div>
	  </br></br></br>
    </h2>
    <form class="ui large form" action="inscription.php" method="post" ENCTYPE="multipart/form-data">>
      <div class="ui stacked segment">
	  
	     <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="text" name="pseudo" placeholder="pseudo" />
          </div>
        </div>	
		
		<div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="text" name="nom" placeholder="nom" />
          </div>
        </div>

        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="text" name="prenom" placeholder="prenom" />
          </div>
        </div>
		
		<div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="date" name="date de naissance" placeholder="Date de naissance" />
          </div>
        </div>
	  
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="e_mail" name="e_mail" placeholder="adresse mail" />
          </div>
        </div>
		
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="text" name="activite" placeholder="activite"/>
          </div>
        </div>
		
		<div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
			  <!--<label for="icone">Icône du fichier (JPG, PNG ou GIF | max. 15 Ko) :</label><br />-->
              <input type="file" name="photo" placeholder="photo"/>
          </div>
        </div>
		
		
     
      <input class="ui fluid large teal submit button" type="submit" name="action1" value="Inscription" />
	  
	  
      </div>
     </form>
  </div>
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
</body>
</html>