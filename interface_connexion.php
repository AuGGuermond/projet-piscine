<!DOCTYPE html>
<html>
<head>
	 <title>Accueil</title>
	 <meta charset="utf-8">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.css">
</head>
<body>
<div class = "ui container">
<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <div class="content">
        Connectez vous à votre compte
      </div>
	  </br></br></br>
    </h2>
    <form class="ui large form" action="page de garde.php" method="post">
	  
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
              <input type="e_mail" name="e_mail" placeholder="adresse mail" />
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="text" name="pseudo" placeholder="Nom utilisateur"/>
          </div>
        </div>
      <input class="ui fluid large teal submit button" type="submit" value="valider" />
	   </br></br></br>
      <input class="ui fluid large teal submit button" type="button" value="Inscription" onclick = "window.location='interface_inscription.php'"/>  
	   </br></br></br>
      <input class="ui fluid large teal submit button" type="button" value="Espace statut" onclick = "window.location='interface_admin.php'"/>
	  
      </div>
     </form>
  </div>
</div>
</div>
<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
</body>
</html>